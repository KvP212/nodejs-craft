import './craft/roboMouse'
import { testTaskPromise } from './fundamentals/microTask-macroTask'
import { generatePasswordPromise } from './craft/generatePasswords'

//Main del node-craft
const main = async () => {

    console.time('Generando Pass...')
    const characterSelect = await generatePasswordPromise(14);
    console.log('Pass Generada characterSelect:', characterSelect)
    
    await testTaskPromise();
}

//Exec main
main();
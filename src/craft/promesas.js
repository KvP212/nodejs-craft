/**
  let x = 10

  const p = new Promise((resolve, reject) => {
    if (x == 10) {
      resolve('La variable es igual a 10')
    } else {
      reject('La variable no es igual a 10')
    }
  })

  p
    .then(res => {
      console.log(`success: ${res}`)
    })
    .catch(err => {
      console.log(`Error: ${err}`)
    })
*/


/**
 * Structure Promise
 * new Promise((resolve, reject) => {}
 * .then(res => { console.log(`success: ${res}`) })
 * .catch(err => { console.log(`Error: ${err}`) })
*/
export const promesaFunc = (index,value) => {

  return new Promise((resolve, reject) => {

    setTimeout(() => {

      if (!value || isNaN(value)) {
        reject('value error')
      }

      value = value * 3 + 2
      console.log(`${index}. proceso terminado...`)
      resolve(value)
    }, 2000)

  })

}


export default async function main() {

  //then
  console.log('11. proceso iniciado...')
  promesaFunc(22, 10)
    .then(res => console.log(`33. el resultado es: ${res}`))
    .catch(err => console.log(`Error: ${err}`))

  //await
  console.log('1. proceso iniciado...')
  try {
    const res = await promesaFunc(2, 34)
    console.log(`3. el resultado es: ${res}`)
  } catch (error) {
    console.log(`Error: ${error}`)
  }

}

//Se envia parametro [length] para el tamaño de contraseña
const generatePassword = (length) => {
    let password = '';
    const characters = 'ABCDEFGHJKMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#$+=.?';
    password += characters[Math.floor(Math.random() * 26) + 26];

    for (let i = 1; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        password += characters[randomIndex];

        if ((i + 1) % 4 === 0 && i !== length - 1) {
            password += '-';
        }
    }

    return password;
}

// Envolviendo la funcion en una promesa
 export const generatePasswordPromise = (length)=>{ 
  return new Promise((resolve, reject) =>{
    resolve(generatePassword(length));
  })
}
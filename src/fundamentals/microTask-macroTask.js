//(Macro)task   setTimeout | setInterval | setImmediate
//Microtask     process.nextTick | Promise callback | queueMicrotask


 const testTask = ()=>{ 
  console.log('start!')

  setTimeout(() => {
      console.log('Timeout')
  }, 0)

  Promise.resolve('Promise!')
  .then(res => console.log(res))

  console.log('End!')
}

// Envolviendo la funcion en una promesa
 export const testTaskPromise = ()=>{ 
  return new Promise((resolve, reject) =>{
    testTask();
    resolve();
  })
}
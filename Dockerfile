FROM node:14.16.1-alpine3.13

#Create app directory
WORKDIR /usr/nodejs-craft
COPY . .
#COPY .package*.json ./
RUN npm install

EXPOSE 4040

CMD [ "npm", "run-script", "dev"]
